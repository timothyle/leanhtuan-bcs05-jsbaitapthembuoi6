function kiemtrasnt(x)
{
    var songuyento = true;
 
    if (x < 2) {
        songuyento = false;
    }
    else if (x == 2) {
        songuyento = true;
    }
    else if (x % 2 == 0) {
        songuyento = false;
    }
    else {
        for (var i = 3; i <= Math.sqrt(x); i += 2)
        {
            if (x % i == 0) {
                songuyento = false;
                break;
            }
        }
    }
 
    return songuyento;
}

function ketQua()
{
    var number = document.getElementById("txt-number").value;
    number = parseInt(number);
 
    var dayso = '';
    for (var i = 1; i <= number; i++) {
        if (kiemtrasnt(i)){
            dayso += i + ' <br/>';
        }
    }
    document.getElementById("result").innerHTML = dayso;
}